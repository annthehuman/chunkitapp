1 For democracy to work, you need prosperity first.
2 It is debatable whether access to information technology has changed women's lives significantly.
3 New species appear when a larger population splits into several smaller populations. 
4 The wings of the birds and human arms do not have the same genetic origin.
5 Populations of prey and predators grow and decline in cycles.
6 There are things which are understandable only in their historical context.
7 The president does not need to report how much he owns.
8 The  world summit needs to be open to the general public.
9 The thesis field work is done in the first year of the programme.
10 If you write a good product plan in your proposal, nobody else can do it in the lab.
11 Traditional knowledge is a well-defined term in this field.
12 Teaching at universities can be done not only by members of staff but also by other students.
13 Whether or not two things happen at the same time depends on the frame of reference.
14 Agreements between EU and Russia on energy affect international economics.
15 The cell actively participates in the process of its own death.
16 We need democratic procedures for taking decisions about the future of the information society.
17 Change has been seen as a favourable thing.
18 The concept of evolution is central for understanding all biological processes.
19 Important stimuli for the brain are those that change.
20 People's legal conciousness may suffer when a law is created but not implemented.
21 The church should be involved in issues of social well-being.
22 Natural selection does not change an individual but it determines whether the individual will live and reproduce.
23 Democracy enables economic success, because it provides both liberty and predictability
24 The businesswomen's first marketing tool was a website.
25 If the result of the act conforms to your expectations, the knowledge is verified.
26 The idea to build a university in Lahti came from Helsinki.
27 In Asia, local communities are in charge of the forest resources.
28 The department does not support international mobility.
29 There is a relationship between the shape and size of a leaf and the climate it grows in.
30 The speaker does not know how to work out which year is a leap year.
31 Having Canada geese in the parks may have unpleasent consequences.
32 Finland is the leading country by the number of volunteers in peacekeeping missions.
33 What we can see in a culture is only the tip of an iceberg.
34 The town of Lahti wanted a veterinary college but failed on account of its poor connections.
35 Growing tomatoes in California has no problems.
36 Most Western societies share similar relationship to the body.
37 People in post-war Sudan have lost all their income from agriculture.
38 Electric field measurement doesn't always provide accurate values.
39 A concept can come to mind while the mind is concentrating on something else.
40 Paying attention is required as anyone can be asked what was just said.
41 Most of the survey results were unexpected.
42 Fast intervention is possible due to the computerised system.
43 The migrants are too anxious to organize happenings in the village.
44 Leap years are necessary for calendar to work.
45 To leave now is the better option than to wait.
46 The countries do not trust each other enough to share information.
47 People know their actual competences well.
48 An industrial town cannot benefit from an institution of higher education.
49 Canada and Cuba are the two developed countries in that area.
50 Experience in a field is not as important as enthusiasm when it comes to solving problems.
51 In interdisciplinary work it is easy to communicate.
52 The animal kingdom is nested inside the plant kingdom.
53 Author should describe the characters in utmost detail.
